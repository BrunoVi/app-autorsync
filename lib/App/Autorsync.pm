package App::Autorsync;
use strict;
use warnings;

# ABSTRACT: automatically sync folders

use Moo;

use Path::Class;
use Scalar::Util 'blessed';
use IPC::Cmd 'can_run';
use List::MoreUtils 'uniq';
use Carp 'croak';

use Filesys::Notify::Simple;

# Origin and destination directories
has from => (
    is       => 'ro',
    coerce   => sub { dir( @_ ) },
    required => 1,
);

has to => (
    is       => 'ro',
    coerce   => sub { dir( @_ ) },
    required => 1,
);

has host => ( is => 'ro' );

# Options to pass to rsync.
# Default:

# --delete so that files deleted at source are deleted at destination.
# -d so that --delete is allowed, basically it will create new
# directories
# -v for verbosity
# -z to compress before sending, always a good idea on slow connections
# Coerce form a string of comma or space-separated list of options, e.g:
# "-a -b --c=d" -> ['-a', '-b', '--c=d' ]

has rsync_options => (
    is  => 'ro',
    isa => sub {
        ref $_[0] eq 'ARRAY'
        or croak "rsync_options must be an array reference"
    },
    coerce  => sub { return ref $_[0] eq 'ARRAY' ? $_[0] : [ split /,| /, $_[0] ] },
    default => sub { ['-v', '-z', '-d', '--delete'] },
);

# Regexp of paths that shouldn't be looked at
has ignore => (
    is       => 'ro',
    coerce   => sub { qr/$_[0]/ },
    isa      => sub { ref $_[0] eq 'Regexp' or croak "ignore should be regexp" }
);

# Path to the rsync executable
has rsync_path => (
    is      => 'ro',
    isa     => \&isa_executable,
    default => sub { 'rsync' },
);

has verbose => ( is => 'ro', default => sub { 0 } );

# The Filesys::Notify::Simple watcher
has _watcher => ( is => 'ro', lazy => 1, builder => '_build_watcher' );

# What to do when a bunch of files have changed
has _on_change => ( is => 'ro', lazy => 1, builder => '_build_cb' );

sub watch {
    my $self = shift;

    $self->p(
        sprintf( "Watching %s, syncing to %s",
            $self->from,
            ( $self->host ? $self->host . ':' . $self->to : $self->to ))
    );

    # Wait for changes.
    $self->_watcher->wait( $self->_on_change ) while 1;
}

sub _build_watcher {
    my $self = shift;

    my $dir = $self->from;

    -d $dir or croak "$dir does not exist or is not a directory.";

    return Filesys::Notify::Simple->new( [$dir] );
}

sub _build_cb {
    my $self = shift;

    return sub {
        # We receive the path of files that changed.
        my @files = map { file( $_->{path} ) } @_;

        # Ignore events from files we don't care about.
        if ( defined $self->ignore ) {
            @files = grep { $_ !~ $self->ignore } @files;
        }

        # Get a unique list of the paths
        my @from_dirs = uniq map { $_->dir } @files;

        for my $from (@from_dirs) {

            # Build the destination path to update
            my $to = $self->_destination_dir($from);

            # Prepend the host destination if there is one
            $to = $self->host . ':' . $to if $self->host;

            $self->sync( $from, $to );
        }
    };
}

sub sync {
    my ($self, $from, $to) = @_;

    $from ||= $self->from;
    $to   ||= ($self->host ? $self->host . ':' : '') . $self->to;

    my @cmd = $self->_command( $from, $to );

    $self->p( join ' ', @cmd );

    system(@cmd);
}

# Helper method that constructs the full path for the destination dir.
# It takes a Dir instance of the path to the changed file and returns a
# Dir instance of the destination directory to update.

sub _destination_dir {
    my ( $self, $dir ) = @_;

    croak "Dir is not defined or is not a Path::Class::Dir instance"
      unless defined $dir && blessed $dir && $dir->isa("Path::Class::Dir");

    my @orig_dirs = $self->from->absolute->dir_list;
    my @dest_dirs = $dir->dir_list;

    @dest_dirs = @dest_dirs[ @orig_dirs .. @dest_dirs - 1 ];

    my $dest_dir = dir( $self->to, @dest_dirs );

    return $dest_dir;
}

sub _command {
    my ( $self, $from, $to ) = @_;

    # We need a trailing '/' in the 'from' path for the command to work
    # properly, a way of doing this portably is stringifying a file with
    # no name.
    $from = file($from, '');

    return ( $self->rsync_path, @{ $self->rsync_options }, $from, $to );
}

sub p {
    my $self = shift;
    print @_, $/ if $self->verbose;
}

sub isa_executable {
    croak "$_[0] is not a valid executable" unless can_run( $_[0] );
}

1;
