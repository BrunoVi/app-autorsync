use strict;
use warnings;
use Test::More;
use Test::Exception;
use Path::Class;

use IPC::Cmd 'can_run';

use_ok 'App::Autorsync';

dies_ok { App::Autorsync->new } 'Constructor dies with no args';
dies_ok { App::Autorsync->new( from => 'foo' ) } "Constructor dies with no 'to'";
dies_ok { App::Autorsync->new( to   => 'foo' ) } "Constructor dies with no 'from'";

throws_ok {
    App::Autorsync->new( from => unexistent_dir(), to => 'to' )->_watcher
} qr/not exist/, "Constructor dies with unexistent dir";


throws_ok {
    App::Autorsync->new(
        from             => 'foo',
        to               => 'bar',
        rsync_path => unexistent_executable(),
    );
} qr/not a valid executable/, "Constructor dies with unexistent executable";

my $sync;

lives_ok {
    $sync = App::Autorsync->new(
        from => '.',
        to   => 'to',
        host => 'user@host',
        rsync_options => [ 'foo', 'bar' ],
    );
} 'Constructor lives with required arguments';

isa_ok $sync, 'App::Autorsync';

isa_ok $sync->from, 'Path::Class::Dir', "Coercion of 'from' works";
isa_ok $sync->to,   'Path::Class::Dir', "Coercion of 'to' works";
is ref $sync->rsync_options, 'ARRAY', 'rsync_options is an arrayref';
is_deeply $sync->rsync_options, [ 'foo', 'bar' ], 'rsync options are handled correctly';

isa_ok $sync->_watcher, 'Filesys::Notify::Simple', "_watcher is a Filesys::Notify::Simple instance";
is ref $sync->_on_change, 'CODE', '_on_change is a code reference';

lives_ok { $sync = App::Autorsync->new(
    from          => 'foo',
    to            => 'bar',
    rsync_options => 'baz,bar foo',
) } "Constructor works with a string in 'rsync_options'";

is ref $sync->rsync_options, 'ARRAY', 'rsync_options is an arrayref';
is_deeply $sync->rsync_options, [ 'baz', 'bar', 'foo' ], 'rsync options are handled correctly';

my @input = (
    {
        from        => dir( '', 'foo')->absolute,
        to          => dir( 'bar', 'baz' ),
        file        => file( '', 'foo', 'path', 'to', 'file.txt' ),
        destination => dir('bar', 'baz', 'path', 'to'),
    }
);

for my $input (@input) {
    my $sync = App::Autorsync->new( from => $input->{from}, to => $input->{to} );

    my $dest = $sync->_destination_dir( $input->{file}->dir );

    isa_ok $dest, 'Path::Class::Dir';

    is $dest, $input->{destination},
      sprintf( "Changed: %s, updating %s", $input->{file}, $dest );
}

sub unexistent_dir {

    # Create a 10-letter random string and check that it is in fact not an
    # existing directory.
    my $dir;

    do { $dir = random_string(); } while ( -d $dir );

    return $dir;
}

sub unexistent_executable {

    # Create a 10-letter random string and check that it is in fact not an
    # existing directory.
    my $exe;

    do { $exe = random_string(); } while ( can_run( $exe ));

    return $exe;
}

sub random_string {
    my @alphabet = ('a'..'z');
    return join '', map { $alphabet[ rand @alphabet ] } (1..10);
}

done_testing();

